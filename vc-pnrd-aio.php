<?php

/*
  Plugin Name: PostNord Delivery Checkout - KaaosFork
  Plugin URI: https://gitlab.com/askolsam/kaaos-postnord-delivery-checkout
  Description: PostNord Delivery checkout integration for WooCommerce. Fork of https://wordpress.org/plugins/vconnect-postnord-delivery-checkout/ with bug fixes and functional patches.
  Version: 2.9.5.0.0-3kaaos
  Author: Kaaos Unlimited Oy
  Author URI: https://www.kaaosunlimited.fi
  Requires at least: 5.4.2
  Tested up to: 5.5.3
 *
  WC requires at least: 4.2.2
  WC tested up to: 4.7.1
 */

/**
 * Check if WooCommerce is active
 */
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    define('AINO_PLUGIN_VERSION', '2.9.5.0.0-3kaaos');

    define('AINO_PLUGIN_PATH', plugin_dir_path(__FILE__));
    define('AINO_PLUGIN_URL', plugin_dir_url(__FILE__));

    require_once 'kaaos-pnrd-utils.php';
    require_once 'core/vc-aio-core.php';
    require_once 'spec/vc-aino-shipping-methods.php';
}